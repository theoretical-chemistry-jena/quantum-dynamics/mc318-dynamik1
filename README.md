# Quantum dynamics practical, part 1: Introduction

This practical course exercise is part of the Theoretical Chemistry course MC3.1.8 at the [Friedrich Schiller University in Jena, Germany](https://uni-jena.de) in the M.Sc. Chemistry program. Information for students and the necessary work sheet can be found at [moodle](https://moodle.uni-jena.de/mod/assign/view.php?id=536624).

## Prerequisites

For convenience, this repo comes with a `shell.nix` file that can be used together with the purely functional package manager [`Nix`](https://nixos.org) on Unix-oid systems (macOS, Linux, WSL on Windows 10+). You can find installation instructions [here](https://nixos.org/download#nix-quick-install).

After installing Nix (and potentially restarting your shell), you can run (from the root directory):

```bash
nix-shell
```

to drop you into an interactive shell with all the necessary software. The first invocation may take some time and requires network access, future invocations will not (unless you changed something in the `shell.nix` file).

## Building and running

The Fortran source code is found in the `src` folder and running

```bash
make
```

from the root directory will build the program (interested people look at the `Makefile` for further information) called `dynamik1`. You can then run the program using

```bash
./dynamik1
```

For quick analysis of the propagation results, you may use either `gnuplot` or Python. The best place to start your analysis is the `out/dens.dat` file which contains the wavefunction propability density $|\Psi(x,t)|^2$ as a 3D dataset.

### Analysis with `gnuplot`

Within the gnuplot command line interface, run:

```gnuplot
> set pm3d map
> splot 'out/dens.dat'
```

### Analysis with Python

For convenience, a Jupyter notebook called `analysis.ipynb` is provided. In its first cell you find two helper functions, of which `load_density()` will help you open the `out/dens.dat` file.

### Changing parameters

To change parameters, feel free to edit the file `input` in the root directory and re-run the simulation using the same command.
