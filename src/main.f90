module data_grid
    integer:: Nt           ! Anzahl der Zeitschritte
    integer:: param         ! Optionsparameter
    integer:: vstates         ! Anzahl der Vibrationszustaende
    integer, parameter:: initpot = 1 !Startpotential und Potentialanzahl
    integer, parameter:: NPOT = 1
    integer, parameter:: Nr = 512       ! Anzahl der Punkte des Ortsgrids
    double precision:: dR, dpR, dt       ! Schrittgroesse Orts-, Impuls- und Zeitgrid
    double precision:: R_init, kappa       ! Parameter der Anfangswellenfunktion
    double precision:: PR(NR)           ! Impulsgrid
    double precision:: Pot(NR)    ! Die Potentialkurven
    double precision:: mass       ! Die Masse des Systems
end module

module data_au
    double precision, parameter:: au2a = 0.52917706d0  ! Umrechnungsfaktor Laenge in a.u. --> Angstrom
    double precision, parameter:: cm2au = 4.5554927d-6 ! Umrechnungsfaktor Energie von Wellenzahlen --> a.u.
    double precision, parameter:: au2fs = 0.024        ! Umrechnungsfaktor Zeit von a.u. --> Femtosekunden
    double precision, parameter:: j2eV = 6.242D18    ! Umrechnungsfaktor Energie von J --> eV
    double precision, parameter:: au2eV = 27.2116d0    ! Umrechnungsfaktor Energie von a.u. --> eV
    double precision, parameter:: i2au = 2.0997496D-9
    double precision, parameter:: pi = 3.141592653589793d0    ! einfach nur pi
    double precision, parameter:: d2au = 0.3934302014076827d0 ! Umrechnungsfaktor Dipolmoment von Debye --> a.u.
    double precision, parameter:: amu = 1836.888d0    ! Atomic mass unit (atomare Masseneinheit)
    double precision, parameter:: mh = 1.d0     ! reduzierte Masse eines H atoms
    complex*16, parameter:: im = (0.d0, 1.d0)      ! Das ist i, die imaginaere Zahl
end module

module pot_param
    use data_au
    double precision, parameter:: R0 = 0.10d0     ! Grid-Parameter, Anfang..
    double precision, parameter:: Rend = 32.d0    !..und Ende
end module pot_param

program wavepacket

    use data_grid
    implicit none

    ! double precision, allocatable, dimension(:):: psi0
    ! double precision, allocatable, dimension(:, :):: phi_vib

    print *
    print *
    print *, 'Initialization...'
    print *

    ! Make the output folder, if not already existent
    call system('mkdir -p out/')

    call input
    call p_grid

    call potential
    call propagation

    print *, 'Finished'

end program

! _______________ Subroutines __________________________________________________

subroutine input

    use data_grid
    use pot_param
    implicit none

    open (10, file='input', status='old')

! In diesem Unterprogramm werden Parameter eingelesen und in atomare Einheiten
! umgerechnet.
! Parameter: Eigenschaften der Anfangswellenfunktion

    read (10, *) Nt            ! Nt = number of time steps.
    read (10, *) R_init        ! R_init = Mittelpunkt der Anfangswellenfunktion
    read (10, *) kappa         ! kappa = Breite der Anfangswellenfunktion
    read (10, *) param         ! Option: 1 = Doppelminimum-Pot, 2 = Morse

    dR = (Rend - R0) / (NR - 1)    ! Schrittweite des Ortsgrids
    mass = 0.5 * mh * amu      ! Reduzierte Masse des Systems
    dt = 0.005d0 / au2fs      ! Zeitschritt
    R_init = R_init / au2a       ! Mittelpunkt der Anfangswellenfunktion

    print *, '_________________________'
    print *
    print *, 'Parameters'
    print *, '_________________________'
    print *
    print *, 'dt = ', SNGL(dt), 'a.u.'
    print *, 'dR = ', SNGL(dR), 'a.u.'
    print *
    if (param .eq. 1) then
        print *, 'Option: Doppelminimum-Potential'
    else if (param .eq. 2) then
        print *, 'Option: H_2+ Molekuel'
    else
        print *, 'Ungueltige Wahl, nur 1 oder 2 zulaessig'
        stop
    end if
    print *
    print *, 'Anfangswellenfunktion:'
    print *, 'R_init:', sngl(R_init), 'a.u.'
    print *, 'Breite:', sngl(kappa), 'a.u.'
    print *
    print *, '__________________________'
    print *

    close (10)
end subroutine

!...................... Impulsgrid......................

subroutine p_grid

    use data_grid
    use data_au
    implicit none
    integer:: I

! In diesem Unterprogramm wird das Grid im Impulsraum definiert. Auf diesem Grid
! ist die Wellenfunktion im Impulsraum definiert - die Schrittgroesse dPR haengt
! von der Schrittgroesse im Ortsraum, dR, und der Anzahl der Gridpunkte NR zusammen.

    dPR = (2.d0 * pi) / (dR * NR)

    do I = 1, NR
        if (I .le. (NR / 2)) then
            PR(I) = (I - 1) * dPR
        else
            PR(I) = -(NR + 1 - I) * dpR
        end if
    end do

    return
end subroutine

!........................... Einlesen des Potentials..............

subroutine potential

    use data_grid
    use data_au
    use pot_param

    implicit none

    integer:: i, j
    double precision:: R, gm

    open (20, file='out/Potential.dat', status='unknown') ! Auswahl des Grundzustands

! Hier wird das Potential, auf dem wir die Kerndynamik berechnen wollen,
! initialisiert. Des weiteren werden die Dipole und Uebergangsdipole angegeben
! und alles in atomare Einheiten umgerechnet.

    pot = (0.d0)

    if (param == 1) then
        do i = 1, nr
            r = r0 + (i - 1) * dr
            pot(i) = 0.2d0 * (0.4d0 * (r - 6)**4 - 5.d0 * (r - 6)**2)
        end do
    else
        open(21, file="H2+_Pot.inp", action="read")
        do i = 1, nr
            read(21,*) r, pot(i)
        end do
        close(21)
    end if

    pot = pot / au2eV

    gm = 1.d6
    do i = 1, Nr
        if (pot(I) .le. gm) then
            gm = pot(i)
        end if
    end do

    do j = 1, 2
    do i = 1, Nr
        pot(i) = pot(i) - gm
    end do
    end do

    do I = 1, NR
        R = R0 + (I - 1) * dR
        write (20, *) sngl(R * au2a), sngl(pot(i) * au2eV)
    end do

    close (20, status='keep')

    return
end subroutine


subroutine eigenvalue_init(A, B, E, dt2)

    use data_grid
    implicit none
    double precision:: e1, e2
    double precision, intent(in):: dt2
    complex*16:: norm
    complex*16, intent(in):: A(nr), B(nr)
    double precision, intent(out):: E

    ! Berechnen der Eigenwerte der propagierten Wellenfunktion

    call overlap(B, B, norm)  ! Berechnet den Ueberlapp zweier Wellenfunktionen
    e1 = abs(norm)

    call overlap(A, A, norm)
    e2 = abs(norm)

    E = (-0.5d0 / dt2) * log(e2 / e1)

    return
end subroutine

!...............................................

subroutine overlap(A, B, norm)

    use data_grid
    implicit none
    integer I
    complex*16, intent(in):: A(Nr), B(Nr)
    complex*16, intent(out):: norm

    ! Das ist der Ueberlapp zweier Wellenfunktionen

    norm = (0.d0, 0.d0)

    do I = 1, Nr
        norm = norm + conjg(A(I)) * B(I)
    end do

    norm = norm * dr

    return
end subroutine

