subroutine propagation()

    use data_grid
    use pot_param
    use data_au
    use omp_lib
    use, intrinsic:: iso_c_binding
    implicit none

    include 'fftw3.f03'

    integer I, J, K

    type(C_PTR):: planF, planB

    double precision:: w
    double precision:: time, tts, tte    ! nun - die Zeit
    double precision:: R      ! Kernabstand
    double precision:: cpm      ! Abschneideparameter
    double precision:: norm    ! die Gesamtnorm der do j = 1, Npot
    double precision:: norm_vib(vstates)  ! Besetzundo j = 1, Npotstaende
    double precision:: evR      ! Ortserwartungswertdo j = 1, Npot
    ! double precision, intent(in):: psi0(nr), phi_vib(nr, vstates)
    double precision:: cof(nr)        ! Abschneidefunktion
    double precision ::normpot

    complex*16, allocatable, dimension(:):: psi    ! Wellenfunktion
    complex*16, allocatable, dimension(:):: kprop    ! Kinetischer Propagator
    complex*16, allocatable, dimension(:):: vprop    ! Potentieller Propagator

    open (100, file='out/psi0.dat', status='unknown')     ! Anfangswellenfunktion - 2D
    open (101, file='out/cut_off_function.dat', status='unknown') ! Die Abschneidefunktion - 2D
    open (200, file='out/dens.dat', status='unknown')       ! Kernwellenpaket im Grundzustand - 3D
    open (800, file='out/R.dat', status='unknown')          ! Ortserwartungswert - 2D, eine Spalte
    open (908, file='out/norm.dat', status='unknown')       ! Norm im el. Zust.- 2D, eine Spalte

    allocate (psi(NR), kprop(NR), vprop(NR))

    call dfftw_plan_dft_1d(planF, nr, psi(:), psi(:), FFTW_FORWARD, FFTW_MEASURE)
    call dfftw_plan_dft_1d(planB, nr, psi(:), psi(:), FFTW_BACKWARD, FFTW_MEASURE)

    psi = (0.d0, 0.d0)      ! Initialisierung

    do I = 1, NR
        R = R0 + (i - 1) * dR
        psi(I) = exp(-kappa * (R - R_init)**2)  ! Einsetzen der Anfangswellenfunktion
    end do

    cpm = 1.5d0 / au2a

    do j = 1, NR      ! Das ist eine Abschneidefunktion, die verhindern soll,
        R = R0 + (j - 1) * dR  ! dass ein Wellenpaket, wenn es dissoziiert, an das Ende
        if (R .lt. (Rend - cpm)) then  ! des Grids stoesst.
            cof(j) = 1.d0    ! Daher schneidet diese Funktion (cut-off-function) die
        else      ! auslaufende Wellenfunktion sanft ein paar Angstrom
            cof(j) = cos(((R - Rend + cpm)/-cpm) * (0.5d0 * pi))  ! (definiert durch den
            cof(j) = cof(j)**2    ! Parameter CPM) vor dem Gridende vorher ab.
        end if
        write (101, *) sngl(R * au2a), sngl(cof(j))
    end do

    kprop = (0.d0, 0.d0)
    vprop = (0.d0, 0.d0)

    do j = 1, NPOT
        do i = 1, NR
            kprop(i) = exp(-im * dt * Pr(i)**2 / (2.d0 * mass)) ! Kinetischer Propagator
            vprop(i) = exp(-im * 0.5d0 * dt * pot(i))  ! Potentieller Propagator dt/2
        end do
    enddo

    call integ(psi, norm)   ! Berechnet die Norm (fuer komplexe Funktionen)
    print *, 'norm =', sngl(norm)
    psi(:) = psi(:) / sqrt(norm)
    call integ(psi, norm)
    print *, 'norm =', sngl(norm)

    do I = 1, NR
        R = R0 + (I - 1) * dR
        ! Rausschreiben des Anfangszustands
        write (100, *) sngl(R * au2a), sngl(abs(psi(I))**2)
    end do

    ! call pop_analysis(phi_vib, psi(initpot, :), norm_vib)
    ! do I = 1, vstates
    !    write (909, *) i, sngl(norm_vib(i))  ! Schreibt die Besetztung der Vibrationszustaende raus
    ! end do
    ! write (909, *) 'Sum:', sngl(sum(norm_vib(1:vstates)))

!______________________________________________________________________
!
!                   Propagation Loop
!______________________________________________________________________

    print *
    print *, '1D propagation...'
    print *

    timeloop: do K = 1, Nt      ! Zeitschleife

        tts = omp_get_wtime()

        time = K * dt

        ! ============= Propagationsteil ==============================================

        psi(:) = vprop(:) * psi(:)

        ! ......Kinetische Propagation ....................
        call fftw_execute_dft(planF, psi, psi)   ! FT: Psi(P) --> Psi(R)

        psi(:) = kprop(:) * psi(:)         ! Kinetische Propagation

        call fftw_execute_dft(planB, psi, psi)     ! FT: Psi(P) --> Psi(R)

        ! Renormierung
        psi(:) = psi(:) / dble(NR)

        ! ......Potentielle Propagation ........................
        psi(:) = vprop(:) * psi(:)
        psi(:) = psi(:)*cof(:)

        ! ============ Ende der Propagation, hier kommt nur noch Output ==============

        ! Berechnet die Norm im elektronischen
        call integ(psi, normpot)

        ! Ortserwarungswert: <R> = int R * |psi(R)|^2 * dR / int |psi(R)|^2 * dR
        evR = 0.d0
        do I = 1, NR
           R = R0 + (I - 1) * dR
            evR = evR + abs(psi(I))**2 * R
        end do
        evR = evR * dR        ! Ortserwartungswert  *dR
        evR = evR / normpot               !  / int |psi(R)|^2 *dR

        ! Schreibt den Ortserwartungswert raus
        write (800, *) sngl(time * au2fs), sngl(evR * au2a)
        ! Schreibt die Population im Zustand
        write (908, *) sngl(time * au2fs), sngl(normpot)
        ! Schreibt das elektrische Feld im jeweiligen Zeitschritt

        if (mod(K, 100) .eq. 0) then   ! Nur jeden 100. Zeitpunkt, sonst Datei zu groß
            do I = 1, NR / 3
                R = R0 + (I - 1) * dR
                write (200, *) sngl(time * au2fs), sngl(R * au2a), &
                 &(sngl(abs(psi(I)**2)))
            end do
            write (200, *)
        end if

        tte = omp_get_wtime()

        if (mod(K, 500) .eq. 0) then
            write (*, *) "Time: ", sngl(time * au2fs), &
             &"Norms: ", sngl(normpot), &
             &"Exec time: ", sngl((tte - tts) * 1.d3), "ms"
        end if

    end do timeloop         ! Ende der Zeitschleife

    close (100, status='keep')
    close (200, status='keep')
    close (800, status='keep')
    close (908, status='keep')

    deallocate (psi, kprop, vprop)

    return
end subroutine

!_________________________________________________________

subroutine integ(psi, norm)

    use data_grid
    implicit none
    integer:: I
    double precision, intent(out):: norm
    complex*16, intent(in):: psi(NR)

! Dieses Unterprogramm rechnet die Norm der komplexen Wellenfunktion aus.

    norm = 0.d0

    do I = 1, NR
        norm = norm + abs(psi(I))**2
    end do

    norm = norm * dR

    return
end subroutine

!______________________________________________________________

subroutine pop_analysis(phi_vib, psi, vib_norm)

    use data_grid
    implicit none
    integer:: i, j
    double precision, intent(out):: vib_norm(vstates)
    double precision, intent(in):: phi_vib(nr, vstates)
    complex*16:: norm(vstates)
    complex*16, intent(in):: psi(NR)

! Dieses Unterprogramm rechnet den Ueberlapp der Wellenfunktion aus.

    vib_norm = 0.d0
    norm = (0.d0, 0.d0)

    do j = 1, vstates
        do I = 1, NR
            norm(j) = norm(j) + phi_vib(i, j) * psi(i)
        end do
        norm(j) = norm(j) * dR
    end do

    return
end subroutine

