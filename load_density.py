"""
Just copy the function into your analysis.ipynb file, instead of the old version.
"""

import numpy as np


def load_density(file, n_timesteps=50000, nr_gridpoints=512):
    """
    Load the density from the Fortran output.

    file: Path to the dens.dat output file. For example 'out/dens.dat'
        or 'runX/out/dens.dat'
    n_timesteps: number of timesteps of the simulation (first line
        in the input file)
    nr_gridpoints: Number of gridpoints used in the simulation.
        You should not need to change the nr_gridpoints value,
        and the
    """

    # Get the indices
    r_idx = nr_gridpoints // 3
    t_idx = (n_timesteps) // 100

    # Open and load the data
    np_data = np.loadtxt(file).reshape(r_idx, t_idx, 4, order="F")

    # Print the shape
    print("Shape of the array:", np_data.shape)

    # Slice the data appropiately.
    t = np_data[:, :, 0]
    r = np_data[:, :, 1]
    psi = np_data[:, :, 2:]

    # Delete the raw data array.
    del np_data

    return t, r, psi