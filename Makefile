FC  		= gfortran
FFLAGS		= -O3 -w -fopenmp -march=native ${FFTWFLGS}
LDFLAGS 	= -lm
FFTWFLGS	= -lfftw3 -L${FFTW_LDIR} -I${FFTW_IDIR}
SRCDIR		= ./src
PREFIX		= .

FFTW_DIR    ?= /usr
FFTW_IDIR   ?= $(FFTW_DIR)/include
FFTW_LDIR   ?= $(FFTW_DIR)/lib64

default: dynamik1

dynamik1: main.o propagation.o
	${FC} *.o ${FFLAGS} ${LDFLAGS} -o dynamik1

main.o:	 ${SRCDIR}/main.f90
	${FC} $< ${FFLAGS} -c -o $@

propagation.o:	${SRCDIR}/propagation.f90
	${FC} $< ${FFLAGS} -c -o $@

install: dynamik1
	mkdir -p ${PREFIX}/bin
	mv $< ${PREFIX}/bin

.PHONY: clean
clean:
	rm -f *.o
	rm -f *.mod
	rm -f dynamik1
